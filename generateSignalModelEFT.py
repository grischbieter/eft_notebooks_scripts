import numpy as np
import ROOT as ROOT
from random import choices
from nest_functions import *
import nestpy as nestpy
import sys
import array as arr

###############################################################################
'''This script needs to be run within a nestpy project. 
## Download gitlab.com:grischbieter/nestpy_lz.git and put this script in there#
## Recoil spectra .txt files are from the LZ High-Energy NR Repo '''
###############################################################################

try:
    operator = int(sys.argv[1])
    mass = int(sys.argv[2])
    print( "Generating Model for O"+str(operator)+"s at "+str(mass)+" GeV\n" )
except:
    print(" Usage Error... This Script takes 2 input arguments: EFT Operator Numbers and WIMP Mass [GeV] \n ")
    exit()




def generateModel( operator, mass ):
    
    detector = nestpy.LZ_Detector()
    nc = nestpy.NESTcalc( detector ) 
    detector.set_s2_thr(1.)
    nEvents = 1000000

    if operator < 10:
        opNum = '0'+str(operator)
    else:
        opNum = str(operator)
    try:
        energy_file = np.loadtxt('Recoils/Operator'+str(opNum)+'/dRdEr_EFT_O'+str(opNum)+'s_m'+str(mass)+'GeV_0.txt')
    except:
        print("File not found for Operator %s at %s GeV! " % (opNum, str(mass) ) )
        exit()

    energies, rates = energy_file[:,0], energy_file[:,1]
    binWidth = energies[1]-energies[0]
    
    rate_sum = sum(rates)
    integrated_rate = rate_sum*binWidth
    probs = rates/rate_sum


    #Now we have positions and energies --> Get Yields from NESTpy
    density = nc.SetDensity( detector.get_T_Kelvin(), detector.get_p_bar() ) 
    driftVelocity = nc.SetDriftVelocity(detector.get_T_Kelvin(), density, 310. )
    interactionType = nestpy.INTERACTION_TYPE(0) # Nuclear Recoils


    S1s, S2s, drifts = [], [], []
    nSimulated = 0
    while len(S1s) < nEvents:
         nSimulated += 1
         recoil_energy = choices( energies, weights=probs, k=1)[0]
         quantas = generate_quanta( nc, interactionType, recoil_energy, 310. ) 
         drift = np.random.uniform( 70., 880. )
         radius = np.random.uniform( 0., 688. )
         random_angle = np.random.uniform(0, 2.*np.pi)
         xPos = radius*np.cos( random_angle )
         yPos = radius*np.sin( random_angle )
         random_Z = detector.get_TopDrift() - drift*driftVelocity
         S1 = generate_S1( nc, interactionType, recoil_energy, 310., quantas, xPos, yPos, random_Z, driftVelocity )[5]
         S2 = generate_S2( nc, interactionType, recoil_energy, 310., quantas, xPos, yPos, random_Z, driftVelocity, drift )[7]
         
         if S1 > 0 and S2 > 1.:
             S1s.append( S1 )
             S2s.append( S2 )
             drifts.append( drift )
    

    weights = np.array( [integrated_rate/nSimulated]*len(S1s) )
    
    return S1s, S2s, drifts, weights


S1s, S2s, drifts, weights = generateModel( operator, mass )

outputFileName = 'EFTModel_LZ_O'+str(operator)+'s_'+str(mass)+'GeV.root'
print('Writing to %s' % outputFileName )



outFile = ROOT.TFile( outputFileName, 'recreate' )
t = ROOT.TTree( 'summary', 'summary' )
s1 = arr.array('d', [0.])
s2 = arr.array('d', [0.])
w = arr.array('d', [0.])
dt = arr.array('d', [0.])


t.Branch( 's1', s1, 'S1c_phd/D' )
t.Branch( 's2', s2, 'S1c_phd/D' )
t.Branch( 'drift', dt, 'drift_us/D')
t.Branch( 'weight', w, 'weights/D')

thresh = 0.
for i in range(len(S1s)):
    if float(i)/float(len(S1s))*100. > thresh:
        print( " %i / 100 " % int(thresh) )
        thresh += 5.

    s1[0] = S1s[i]
    s2[0] = S2s[i]
    dt[0] = drifts[i]
    w[0] = weights[i]
    t.Fill()


outFile.Write()
outFile.Close() 

print("Done!" )
exit()







